/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   push_swap.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: thzeribi <thzeribi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/05/14 15:55:38 by thzeribi          #+#    #+#             */
/*   Updated: 2021/10/31 04:31:03 by thzeribi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_push_swap.h"

static void
	algo_selector(t_data *data, t_stack **stack_a)
{
	if (data->stack_a_len == 2)
		small_list_2(stack_a);
	else if (data->stack_a_len == 3)
		small_list_3(stack_a);
	else if (data->stack_a_len == 4)
		small_list_4(stack_a, data);
	else if (data->stack_a_len == 5)
		small_list_5(stack_a);
	else
		large_list_1(stack_a, data);
}

static int
	check_duplicate(t_stack **stack)
{
	t_stack	*tmp1;
	t_stack	*tmp2;

	tmp1 = *stack;
	while (tmp1)
	{
		tmp2 = tmp1->next;
		while (tmp2)
		{
			if (tmp1->nbr == tmp2->nbr)
				return (FALSE);
			tmp2 = tmp2->next;
		}
		tmp1 = tmp1->next;
	}
	return (TRUE);
}

static int
	init_list(char **nbr_lst, t_stack **stack_a)
{
	int		i;
	int		y;

	i = -1;
	while (nbr_lst[++i])
	{
		y = -1;
		while (nbr_lst[i][++y])
		{
			if (ft_isdigit(nbr_lst[i][y]) == 0)
			{
				if ((nbr_lst[i][y] == '-' && y != 0)
					|| (nbr_lst[i][y] == '-' && nbr_lst[i][++y] == 0))
					return (FALSE);
				else if (ft_isdigit(nbr_lst[i][y]) == 0)
					return (FALSE);
			}
		}
		if (ft_check_int_limit(nbr_lst[i]) == FALSE
			|| stack_add(stack_a, ft_atoi(nbr_lst[i])) == FALSE)
			return (FALSE);
	}
	if (check_duplicate(stack_a) == FALSE)
		return (FALSE);
	return (TRUE);
}

int	main(int argc, char *argv[])
{
	t_data	data;
	t_stack	*stack_a;

	if (argc < 2)
		return (0);
	stack_a = NULL;
	init_data(&data);
	if (init_list(++argv, &stack_a) == FALSE)
		exit_error(&stack_a);
	apply_index(&stack_a);
	fill_data(&data, stack_a);
	if (!is_sorted(stack_a))
		algo_selector(&data, &stack_a);
	free_stack(&stack_a);
	return (0);
}
