/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   checker.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tzeribi <tzeribi@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/10/29 09:40:51 by tzeribi           #+#    #+#             */
/*   Updated: 2021/10/29 09:40:57 by tzeribi          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_push_swap.h"

int
	check_if_sort(t_stack *stack)
{
	while (stack)
	{
		if (stack->next != NULL && stack->nbr > stack->next->nbr)
			return (FALSE);
		stack = stack->next;
	}
	return (TRUE);
}
