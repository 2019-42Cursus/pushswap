/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   stack_sort_utils.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tzeribi <tzeribi@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/10/29 10:06:37 by tzeribi           #+#    #+#             */
/*   Updated: 2021/10/29 13:42:26 by tzeribi          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_push_swap.h"

int
	get_smallest_elem(t_stack *stack)
{
	int	min;

	min = stack->nbr;
	stack = stack->next;
	while (stack)
	{
		if (stack->nbr < min)
			min = stack->nbr;
		stack = stack->next;
	}
	return (min);
}

int
	get_biggest_elem(t_stack *stack)
{
	int	max;

	max = stack->nbr;
	stack = stack->next;
	while (stack)
	{
		if (stack->nbr > max)
			max = stack->nbr;
		stack = stack->next;
	}
	return (max);
}

int
	get_stack_len(t_stack *stack)
{
	int	len;

	len = 0;
	while (stack)
	{
		len++;
		stack = stack->next;
	}
	return (len);
}

int
	get_last_elem(t_stack *stack)
{
	while (stack)
	{
		if (stack->next == NULL)
			return (stack->nbr);
		stack = stack->next;
	}
	return (-1);
}
