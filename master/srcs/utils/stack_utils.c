/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   stack_utils.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tzeribi <tzeribi@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/10/29 09:36:42 by tzeribi           #+#    #+#             */
/*   Updated: 2021/10/30 06:27:28 by tzeribi          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_push_swap.h"

int
	stack_add(t_stack **stack, int value)
{
	t_stack	*tmp;

	if (!*stack)
	{
		*stack = (t_stack *)malloc(sizeof(t_stack));
		if (!*stack)
			return (FALSE);
		(*stack)->nbr = value;
		(*stack)->index = -1;
		(*stack)->next = NULL;
	}
	else
	{
		tmp = *stack;
		while (tmp->next)
			tmp = tmp->next;
		tmp->next = (t_stack *)malloc(sizeof(t_stack));
		if (!tmp->next)
			return (FALSE);
		tmp->next->nbr = value;
		tmp->next->index = -1;
		tmp->next->next = NULL;
	}
	return (TRUE);
}

void
	free_stack(t_stack **stack)
{
	t_stack	*tmp;

	tmp = *stack;
	while (tmp)
	{
		*stack = tmp;
		tmp = tmp->next;
		free(*stack);
	}
}

t_stack
	*copy_stack(t_stack **stack)
{
	t_stack	*copy;

	copy = NULL;
	while (*stack)
	{
		stack_add(&copy, (*stack)->nbr);
		*stack = (*stack)->next;
	}
	return (copy);
}

void
	print_stack(t_stack *stack)
{
	while (stack)
	{
		ft_putnbr(stack->nbr);
		if (stack->next != NULL)
			ft_putstr(" | ");
		stack = stack->next;
	}
	ft_putchar('\n');
}
