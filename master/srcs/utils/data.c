/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   data.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tzeribi <tzeribi@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/10/29 09:46:45 by tzeribi           #+#    #+#             */
/*   Updated: 2021/10/29 11:10:41 by tzeribi          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_push_swap.h"

t_data
	init_data(t_data *data)
{
	data->min_value = -1;
	data->big_value = -1;
	data->stack_a_len = 0;
	return (*data);
}

t_data
	fill_data(t_data *data, t_stack *a)
{
	data->min_value = get_smallest_elem(a);
	data->big_value = get_biggest_elem(a);
	data->stack_a_len = get_stack_len(a);
	return (*data);
}
