/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   sort_utils.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tzeribi <tzeribi@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/10/29 11:33:17 by tzeribi           #+#    #+#             */
/*   Updated: 2021/10/30 06:00:11 by tzeribi          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_push_swap.h"

void
	sort_3_number(t_stack **s)
{
	if ((*s)->nbr > (*s)->next->nbr && (*s)->nbr < (*s)->next->next->nbr)
		sa(s);
	else if ((*s)->nbr > (*s)->next->next->nbr && (*s)->nbr < (*s)->next->nbr)
		rra(s);
	else if ((*s)->nbr > (*s)->next->nbr && (*s)->nbr > (*s)->next->next->nbr
		&& (*s)->next->nbr > (*s)->next->next->nbr)
	{
		sa(s);
		sort_3_number(s);
	}
	else if ((*s)->nbr > (*s)->next->next->nbr)
		ra(s);
	else if ((*s)->nbr < (*s)->next->next->nbr
		&& (*s)->next > (*s)->next->next->next)
	{
		sa(s);
		sort_3_number(s);
	}
}

int
	is_sorted(t_stack *stack)
{
	while (stack)
	{
		if (stack->next && stack->nbr > stack->next->nbr)
			return (FALSE);
		stack = stack->next;
	}
	return (TRUE);
}

void
	apply_index(t_stack **stack)
{
	int		index;
	int		nbr;
	t_stack	*tmp1;
	t_stack	*tmp2;

	tmp1 = *stack;
	tmp2 = *stack;
	index = 0;
	while (tmp1)
	{
		index = 0;
		nbr = tmp1->nbr;
		while (tmp2)
		{
			if (nbr > tmp2->nbr)
				index += 1;
			tmp2 = tmp2->next;
		}
		tmp2 = *stack;
		tmp1->index = index;
		tmp1 = tmp1->next;
	}
}
