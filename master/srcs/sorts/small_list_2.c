/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   small_list_2.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tzeribi <tzeribi@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/09/27 22:51:36 by thzeribi          #+#    #+#             */
/*   Updated: 2021/10/29 14:00:44 by tzeribi          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_push_swap.h"

void
	small_list_2(t_stack **stack_a)
{
	if (*stack_a == NULL)
		exit_error(stack_a);
	else
	{
		if ((*stack_a)->nbr < (*stack_a)->next->nbr)
			return ;
		sa(stack_a);
	}
}
