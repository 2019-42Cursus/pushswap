/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   large_list_1.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tzeribi <tzeribi@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/10/23 19:08:20 by thzeribi          #+#    #+#             */
/*   Updated: 2021/10/30 06:55:09 by tzeribi          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_push_swap.h"

static void
	solve_large_1(t_stack **stack_a, t_stack **stack_b, int max_bytes,
	t_data *data)
{
	int		len;
	int		i;
	int		size;
	int		num;

	size = data->stack_a_len;
	len = 0;
	while (len < max_bytes)
	{
		i = 0;
		while (i < size)
		{
			num = (*stack_a)->index;
			if (((num >> len) & 1) == 1)
				ra(stack_a);
			else
				pb(stack_a, stack_b);
			i++;
		}
		while (*stack_b)
			pa(stack_a, stack_b);
		len++;
	}
}

void
	large_list_1(t_stack **stack_a, t_data *data)
{
	int		max_index;
	int		max_bytes;
	t_stack	*stack_b;

	stack_b = NULL;
	max_bytes = 0;
	max_index = data->stack_a_len - 1;
	while (max_index >> max_bytes != 0)
		max_bytes++;
	solve_large_1(stack_a, &stack_b, max_bytes, data);
	free_stack(&stack_b);
}
