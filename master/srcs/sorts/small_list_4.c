/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   small_list_4.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tzeribi <tzeribi@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/10/24 00:09:58 by thzeribi          #+#    #+#             */
/*   Updated: 2021/10/30 06:07:49 by tzeribi          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_push_swap.h"

void
	small_list_4(t_stack **stack_a, t_data *data)
{
	int		i;
	t_stack	*stack_b;

	i = 0;
	stack_b = NULL;
	while (i <= data->stack_a_len)
	{
		if ((*stack_a)->index == 0)
			pb(stack_a, &stack_b);
		else
			ra(stack_a);
		i++;
	}
	sort_3_number(stack_a);
	pa(&stack_b, stack_a);
	free_stack(&stack_b);
}
