/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   small_list_5.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tzeribi <tzeribi@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/10/25 04:36:00 by thzeribi          #+#    #+#             */
/*   Updated: 2021/10/30 11:16:18 by tzeribi          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_push_swap.h"

static void
	resolve_5(t_stack **stack_a, t_stack **stack_b)
{
	int	i;
	int	smallest;

	i = -1;
	while (++i != 2)
	{
		smallest = get_smallest_elem(*stack_a);
		while ((*stack_a)->nbr != smallest)
		{
			if (smallest == get_last_elem(*stack_a))
			{
				rra(stack_a);
				break ;
			}
			else
				ra(stack_a);
		}
		pb(stack_a, stack_b);
	}
	if (is_sorted(*stack_b))
		rb(stack_b);
	sort_3_number(stack_a);
	pa(stack_a, stack_b);
	pa(stack_a, stack_b);
}

void
	small_list_5(t_stack **stack_a)
{
	t_stack		*b;

	b = NULL;
	if (*stack_a == NULL)
		return ;
	resolve_5(stack_a, &b);
	free_stack(&b);
}
