/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   rotate.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tzeribi <tzeribi@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/06/13 16:02:39 by thzeribi          #+#    #+#             */
/*   Updated: 2021/10/30 06:50:30 by tzeribi          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_push_swap.h"

void
	ra(t_stack **stack_a)
{
	t_stack	*tmp1;
	t_stack	*tmp2;

	if (!stack_a || (*stack_a)->next == NULL)
		return ;
	tmp1 = *stack_a;
	*stack_a = tmp1->next;
	tmp2 = *stack_a;
	while (tmp2->next)
		tmp2 = tmp2->next;
	tmp2->next = tmp1;
	tmp1->next = NULL;
	write(1, "ra\n", 3);
}

void
	rra(t_stack **stack_a)
{
	t_stack	*tmp1;
	t_stack	*tmp2;

	if (!stack_a || (*stack_a)->next == NULL)
		return ;
	tmp1 = *stack_a;
	tmp2 = *stack_a;
	while (tmp1->next->next)
		tmp1 = tmp1->next;
	(*stack_a) = tmp1->next;
	tmp1->next = NULL;
	(*stack_a)->next = tmp2;
	write(1, "rra\n", 4);
}

void
	rb(t_stack **stack_b)
{
	t_stack	*tmp1;
	t_stack	*tmp2;

	if (!stack_b || (*stack_b)->next == NULL)
		return ;
	tmp1 = *stack_b;
	*stack_b = tmp1->next;
	tmp2 = *stack_b;
	while (tmp2->next)
		tmp2 = tmp2->next;
	tmp2->next = tmp1;
	tmp1->next = NULL;
	write(1, "ra\n", 3);
}

void
	rrb(t_stack **stack_b)
{
	t_stack	*tmp1;
	t_stack	*tmp2;

	if (!stack_b || (*stack_b)->next == NULL)
		return ;
	tmp1 = *stack_b;
	tmp2 = *stack_b;
	while (tmp1->next->next)
		tmp1 = tmp1->next;
	(*stack_b) = tmp1->next;
	tmp1->next = NULL;
	(*stack_b)->next = tmp2;
	write(1, "rra\n", 4);
}
