/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   swap.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tzeribi <tzeribi@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/06/13 16:01:38 by thzeribi          #+#    #+#             */
/*   Updated: 2021/10/29 11:27:56 by tzeribi          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_push_swap.h"

void
	sa(t_stack **stack_a)
{
	t_stack	*tmp1;
	t_stack	*tmp2;
	t_stack	*tmp3;

	if (!stack_a)
		return ;
	if (get_stack_len(*stack_a) < 2)
		return ;
	tmp1 = *stack_a;
	tmp2 = tmp1->next;
	tmp3 = tmp2->next;
	(*stack_a) = tmp2;
	tmp2->next = tmp1;
	tmp1->next = tmp3;
	write(1, "sa\n", 3);
}

void
	sb(t_stack **stack_b)
{
	t_stack	*tmp1;
	t_stack	*tmp2;
	t_stack	*tmp3;

	if (!stack_b)
		return ;
	if (get_stack_len(*stack_b) < 2)
		return ;
	tmp1 = *stack_b;
	tmp2 = tmp1->next;
	tmp3 = tmp2->next;
	(*stack_b) = tmp2;
	tmp2->next = tmp1;
	tmp1->next = tmp3;
	write(1, "sb\n", 3);
}

void
	swap_double(t_stack **stack_a, t_stack **stack_b)
{
	sa(stack_a);
	sb(stack_b);
	write(1, "ss\n", 3);
}
