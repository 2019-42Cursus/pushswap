# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: thzeribi <thzeribi@student.42.fr>          +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2021/10/12 05:38:54 by thzeribi          #+#    #+#              #
#    Updated: 2021/10/29 03:48:52 by thzeribi         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

ifndef VERBOSE
	MAKEFLAGS += --no-print-directory
endif

NAME				=	project_tester
PROJECT_NAME		=	ProjectTester

SOURCES_FOLDER		=	./srcs/
INCLUDES_FOLDER		=	./includes/
OBJECTS_FOLDER		=	./objs/

SOURCES := \
		main.c \
		test_struc.c

LIBFT = ../libft/

OBJECTS		 =	$(SOURCES:.c=.o)
OBJECTS		:=	$(addprefix $(OBJECTS_FOLDER),$(OBJECTS))
SOURCES		:=	$(addprefix $(SOURCES_FOLDER),$(SOURCES))

FLAGS			=	-Wall -Wextra -Werror
CC				=	gcc


NO_COLOR 		=	\033[38;5;15m
OK_COLOR		=	\033[38;5;2m
ERROR_COLOR		=	\033[38;5;1m
WARN_COLOR		=	\033[38;5;3m
SILENT_COLOR	=	\033[38;5;245m
INFO_COLOR		=	\033[38;5;140m
OBJ_COLOR		=	\033[0;36m

define delete_header
	find $(INCLUDES_FOLDER) ! -name 'ft_test.h' -type f -exec rm -f {} +
endef

all: $(NAME)

$(NAME): $(OBJECTS)
	@printf "\t\t$(NO_COLOR)All objects for $(INFO_COLOR)$(PROJECT_NAME) $(NO_COLOR)where successfully created.\n"
	@make -C $(LIBFT)
	@cp $(LIBFT)libft.a .
	@printf "\t\t$(INFO_COLOR)$(PROJECT_NAME) $(NO_COLOR)Removed all objects$(NO_COLOR).\n"
	@printf "\n\t\t$(NO_COLOR)-------- $(INFO_COLOR) Start Compilation for ${PROJECT_NAME} $(NO_COLOR)--------\n"
	@$(CC) $(FLAGS) -I$(INCLUDES_FOLDER) -o $(NAME) $(OBJECTS) ./libft.a
	@printf "%-50s \r"
	@printf "\t\t\t$(INFO_COLOR)$(NAME) $(NO_COLOR)successfully compiled. $(OK_COLOR)✓$(NO_COLOR)\n"

$(OBJECTS_FOLDER)%.o: $(SOURCES_FOLDER)%.c
	@mkdir -p $(OBJECTS_FOLDER)
	@cp $(LIBFT)/includes/*.h $(INCLUDES_FOLDER)
	@$(CC) $(FLAGS) -I$(INCLUDES_FOLDER) -c $< -o $@
	@printf "%-50s \r"
	@printf "\t\t\t$(NO_COLOR)Creating $(INFO_COLOR)%-30s $(OK_COLOR)✓$(NO_COLOR)\r" "$@"

clean:
	@make -C $(LIBFT) clean
	@rm -f $(OBJECTS)
	@rm -f libft.a
	@$(call delete_header)

fclean: clean
	@make -C $(LIBFT) fclean
	@printf "\t\t$(INFO_COLOR)LibFt $(NO_COLOR)Removed $(INFO_COLOR)Libft$(NO_COLOR).\n"
	@rm -f $(NAME)
	@rm -rf $(OBJECTS_FOLDER)
	@printf "\t\t$(INFO_COLOR)$(PROJECT_NAME) $(NO_COLOR)Removed $(INFO_COLOR)$(NAME)$(NO_COLOR).\n"

norm:
	@printf "\n$(INFO_COLOR)$(PROJECT_NAME) $(NO_COLOR)Norm check$(NO_COLOR).\n\n"
	@norminette {$(INCLUDES_FOLDER),$(SOURCES_FOLDER)}*

re: fclean all

.PHONY: all re clean fclean norm
