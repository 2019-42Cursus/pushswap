/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   test_struc.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tzeribi <tzeribi@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/10/12 04:45:34 by thzeribi          #+#    #+#             */
/*   Updated: 2021/10/29 15:12:17 by tzeribi          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_test.h"

t_test
	**get_all_tests(void)
{
	static t_test	*tests = NULL;

	return (&tests);
}

void
	add_test(t_test **begin_list, char *value, int len)
{
	t_test	*tmp;

	if (*begin_list == NULL)
	{
		*begin_list = malloc(sizeof(t_test));
		(*begin_list)->test = value;
		(*begin_list)->len = len;
		(*begin_list)->next = NULL;
		return ;
	}
	tmp = *begin_list;
	while (tmp->next)
		tmp = tmp->next;
	tmp->next = malloc(sizeof(t_test));
	tmp->next->test = value;
	tmp->next->len = len;
	tmp->next->next = NULL;
}
