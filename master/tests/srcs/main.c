/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: thzeribi <thzeribi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/10/12 04:27:40 by thzeribi          #+#    #+#             */
/*   Updated: 2021/10/29 03:39:53 by thzeribi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_test.h"

static void
	tests_program(char *program)
{
	t_test	**tests;
	char	*command;
	int		full_size;
	int		i;

	tests = get_all_tests();
	i = 1;
	while (*tests)
	{
		full_size = ft_strlen(program) + ft_strlen((*tests)->test);
		command = (char *)malloc(full_size + 1);
		ft_strcpy(command, program);
		ft_strcat(command, " ");
		ft_strcat(command, (*tests)->test);
		printf("--- Test %d ---\n\n", i);
		printf("Test : [%d] %s \n\n", (*tests)->len, command);
		system(command);
		ft_putstr("\n---------------\n\n");
		free(command);
		*tests = (*tests)->next;
		i++;
		sleep(1);
	}
}

static void
	init(void)
{
	t_test	**tests;

	tests = get_all_tests();
}

static int
	check_program(char *program)
{
	FILE	*file;

	if (program[0] != '.' || program[1] != '/')
		return (FALSE);
	file = fopen(program, "r");
	if (file == NULL)
		return (FALSE);
	return (TRUE);
}

int	main(int argc, char *argv[])
{
	if (argc < 2 || argv[1] == NULL )
	{
		ft_putstr("Error : No program specified.\n");
		return (0);
	}
	if (check_program(argv[1]) == FALSE)
	{
		ft_putstr("Error : File not found or invalid.\n");
		return (0);
	}
	init();
	if (*get_all_tests() == NULL)
	{
		ft_putstr("Error : Aucun Tests défini !\n");
		return (0);
	}
	tests_program(argv[1]);
	return (0);
}
