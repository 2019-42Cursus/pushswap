/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_test.h                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: thzeribi <thzeribi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/10/13 02:35:49 by thzeribi          #+#    #+#             */
/*   Updated: 2021/10/15 04:15:43 by thzeribi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_TEST_H
# define FT_TEST_H
# include "libft.h"

typedef struct s_test
{
	char			*test;
	int				len;
	struct s_test	*next;
}				t_test;

t_test	**get_all_tests(void);
void	add_test(t_test **begin_list, char *value, int len);

#endif
