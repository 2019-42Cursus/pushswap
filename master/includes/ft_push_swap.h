/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_push_swap.h                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tzeribi <tzeribi@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/06/15 17:33:20 by thzeribi          #+#    #+#             */
/*   Updated: 2021/10/30 11:10:15 by tzeribi          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_PUSH_SWAP_H
# define FT_PUSH_SWAP_H
# include "libft.h"

typedef struct s_stack
{
	int				nbr;
	int				index;
	struct s_stack	*next;
}				t_stack;

typedef struct s_sort
{
	t_vector		*instruction;
	struct s_sort	*next;
}				t_sort;

typedef struct s_data
{
	int			min_value;
	int			big_value;
	int			stack_a_len;
}			t_data;

t_data		init_data(t_data *data);
t_data		fill_data(t_data *data, t_stack *a);
void		free_data(t_data *data);

t_stack		*copy_stack(t_stack **stack);

int			get_last_elem(t_stack *stack);
int			if_stack_empty(t_stack *stack);
int			stack_add(t_stack **stack, int value);

int			get_smallest_elem(t_stack *stack);
int			get_biggest_elem(t_stack *stack);
int			get_stack_len(t_stack *stack);
void		free_stack(t_stack **stack);
void		print_stack(t_stack *stack);

void		sort_3_number(t_stack **stack);
int			is_sorted(t_stack *stack);
void		apply_index(t_stack **stack);

void		exit_error(t_stack **stack);

void		sa(t_stack **stack_a);
void		sb(t_stack **stack_b);
void		swap_double(t_stack **stack_a, t_stack **stack_b);

void		ra(t_stack **stack_a);
void		rb(t_stack **stack_b);
void		rra(t_stack **stack_a);
void		rrb(t_stack **stack_b);
void		rrr(t_stack **stack_a, t_stack **stack_b);
void		rr(t_stack **stack_a, t_stack **stack_b);

void		pa(t_stack **stack_a, t_stack **stack_b);
void		pb(t_stack **stack_a, t_stack **stack_b);

void		small_list_2(t_stack **stack_a);
void		small_list_3(t_stack **stack_a);
void		small_list_4(t_stack **stack_a, t_data *data);
void		small_list_5(t_stack **stack_a);
void		large_list_1(t_stack **stack_a, t_data *data);

#endif
